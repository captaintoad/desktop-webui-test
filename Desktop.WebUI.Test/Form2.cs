﻿using Gecko;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop.WebUI.Test
{
    public partial class form2 : Form
    {
        public form2()
        {
            InitializeComponent();
            Xpcom.Initialize(Path.Combine(Application.StartupPath, "xulrunner"));
            geckoWebBrowser1.Navigate(Path.Combine(Application.StartupPath, "Pages\\MainGecko.html"));
        }

        private void geckoWebBrowser1_Navigating(object sender, Gecko.Events.GeckoNavigatingEventArgs e)
        {
            if (e.Uri.IsFile && e.Uri.Segments[e.Uri.Segments.Length - 3].CompareTo("app/") == 0)
            {
                e.Cancel = true;

                int lAppStart = e.Uri.OriginalString.IndexOf("app/");
                Uri lNavigateTo = new Uri(e.Uri.OriginalString.Substring(lAppStart).Replace("//","://"));
                Form lFormToOpen = null;

                foreach (Form lForm in Application.OpenForms)
                {
                    if (lForm.Name.CompareTo(lNavigateTo.Host) == 0)
                    {
                        lFormToOpen = lForm;
                    }
                }

                if (lFormToOpen == null)
                {
                    lFormToOpen = (Form)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance("Desktop.WebUI.Test." + lNavigateTo.Host);
                }
                lFormToOpen.Show();
                lFormToOpen.Focus();
            }
        }
    }
}
