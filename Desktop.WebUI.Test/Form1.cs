﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop.WebUI.Test
{
    public partial class form1 : Form
    {
        public form1()
        {
            InitializeComponent();

            webBrowser1.Navigate(Path.Combine(Application.StartupPath, "Pages\\MainIE.html")); //.DocumentText = File.ReadAllText("Pages\\Main.html");
        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.Scheme.CompareTo("app") == 0)
            {
                e.Cancel = true;

                Form lFormToOpen = null;

                foreach (Form lForm in Application.OpenForms)
                {
                    if (lForm.Name.CompareTo(e.Url.Host) == 0)
                    {
                        lFormToOpen = lForm;
                    }
                }

                if (lFormToOpen == null)
                {
                    lFormToOpen = (Form)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance("Desktop.WebUI.Test." + e.Url.Host);
                }
                lFormToOpen.Show();
                lFormToOpen.Focus();
            }
        }
    }
}
