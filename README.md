# README #

This README documents the steps that are necessary to get the application up and running.

### What is this repository for? ###

* Quick summary
This application is a a very simple proof of concept that shows how to host html pages in a desktop application and use them as UI.  More work needs to be done on adding jQuery widgets, styling, etc.

* Dependencies
The application has a dependency on the following:
GeckoFX 29.0.11 - https://bitbucket.org/geckofx/geckofx-29.0/downloads
XulRunner 29.0 - http://ftp.mozilla.org/pub/mozilla.org/xulrunner/releases/29.0/runtimes/

You need to download these and extract the zip files.  Add the contents of the GeckoFX zip to the bin folder directly (no sub folders).  Then add the "xulrunner" folder from the zip file to the bin folder.